import { Component, OnInit } from '@angular/core';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FileUploadService } from 'src/app/services/file-upload.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
// interface ObjectFile extends File {
//   uploadDatetime: Date,
// }
export class FileUploadComponent implements OnInit {
  selectedFiles?: FileList;
  currentFile?: File;
  progress = 0;
  message = '';
  fileContent: any = '';
  fileInfos: Array<any> = [];
  fileTypesAllowed = ['text/plain'];
  chunkSize = 50;

  constructor(private uploadService: FileUploadService) {
  }

  ngOnInit(): void {
  }

  selectFile(event: any): void {
    this.selectedFiles = event.target.files;
    // read file
    // @ts-ignore
    let file = this.selectedFiles[0];

    const isFiletypeAllowed = this.fileTypesAllowed.indexOf(file.type) > -1;
    const isFileDuplicate = this.fileInfos.find(fileObj => fileObj.lastModified == file.lastModified);
    if (isFileDuplicate) {
      // notify the user the file type is not allowed with notification modal
      console.warn(`${file.name} already in the que. Please select another`);
      return;
    }
    const fileObj = Object.assign(file, { uploadDatetime: new Date() });

    this.fileInfos.push(fileObj);
    if (!isFiletypeAllowed) {
      // notify the user the file type is not allowed with notification modal
      console.warn(`${file.type} is not allowed. Please select only types from ${this.fileTypesAllowed}`);
      return;
    }
    this.showFile(file);
  }

  showFile(file: any): void {
    const fileSize = (file.size - 1);

    for(let i =0; i < fileSize; i += this.chunkSize) {
      this.processInChunks(file, i);
    }
  }

  processInChunks(file: any, start: any) {
    let fileReader: FileReader = new FileReader();
    var blob = file.slice(start, this.chunkSize + start);
    // let isImage = file['type'].split('/')[0] == 'image';
    this.fileContent = '';
    fileReader.onload = (x) => {
      this.fileContent += fileReader.result;
    }
    fileReader.readAsText(blob);
  }

/*  upload(): void {
    this.progress = 0;

    if (this.selectedFiles) {
      const file: File | null = this.selectedFiles.item(0);

      if (file) {
        this.currentFile = file;
        console.log(file);

        this.uploadService.upload(this.currentFile).subscribe({
          next: (event: any) => {
            if (event.type === HttpEventType.UploadProgress) {
              this.progress = Math.round((100 * event.loaded) / event.total);
            } else if (event instanceof HttpResponse) {
              this.message = event.body.message;
            }
          }, error: (err: any) => {
            console.log(err);
            this.progress = 0;

            if (err.error && err.error.message) {
              this.message = err.error.message;
            } else {
              this.message = 'Could not upload the file!';
            }

            this.currentFile = undefined;
          },
        });
      }

      this.selectedFiles = undefined;
    }
  }*/
}
